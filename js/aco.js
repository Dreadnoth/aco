/**
 * Znormalizować
 */

function ANT(startNode, endNode, feromoneMatrix, distanceMatrix, feromoneInfluence, distanceInfluence) {
    var currentNode = startNode;
    var endNode = endNode;

    this.search = function() {
        var visitedNodes = new Array();
        var visitedEdges = new Array();
        
        
        do{
            //połączone z naszym punktem krawędzie
            var connectedEdge = new Array();
            //wartości prawdopodobieństw na poszczególnych krawędziach
            var propability = new Array();

            //szukamy połączonych krawędzi z naszym punktem
            sigInst.iterEdges(function(e) {
                if (currentNode === e.source || currentNode === e.target) {
                    if (currentNode === e.source && visitedNodes.indexOf(e.target) === -1) {
                        connectedEdge.push(e);
                    } else if (currentNode === e.target && visitedNodes.indexOf(e.source) === -1) {
                        connectedEdge.push(e);
                    }
                }
            });
            //suma prawdopodobieństw
            var sumOfPropability = 0;
            
            //obliczamy sumę prawdopodobieństw
            for (var i = 0; i < connectedEdge.length; i++) {
                sumOfPropability += Math.pow(feromoneMatrix[connectedEdge[i].id], feromoneInfluence) * Math.pow(1 / distanceMatrix[connectedEdge[i].id], distanceInfluence);
            }
            
            //wylosowana krawędź
            var wylosowana;
            //następny węzeł
            var next = null;
            
            //dodajemy obecny węzęł do tablicy odwiedzonych węzłów
            visitedNodes.push(currentNode);
            
            //losowa krawędź
            var random_edge = null;
            
            //jeżeli na wszystkich krawędziach suma prawdopodobieństw jest równa 0
            //to wybieramy krawędź losowo
            if (sumOfPropability !== 0) {
                //dokonczyc
                for (var i = 0; i < connectedEdge.length; i++) {
                    //!!!!! error!!!!
                    propability.push({"id" : connectedEdge[i].id, 
                        "prop" :Math.round(((Math.pow(feromoneMatrix[connectedEdge[i].id],feromoneInfluence)*Math.pow(1/distanceMatrix[connectedEdge[i].id],distanceInfluence))/sumOfPropability)*100)
                    });
                }
                wylosowana = Math.floor(Math.random() * 100);
                
                propability.sort(function(a,b){
                    return a.prop - b.prop;
                });
                
                for(var i=1; i< propability.length;i++){
                    propability[i].prop = propability[i].prop + propability[i-1].prop;
                }
                
                for(var i=0;i < propability.length && random_edge === null;i++){
                    if(i===0){
                        if(wylosowana >= 0 && wylosowana <= propability[i].prop){
                            random_edge = propability[i].id;
                        }
                    }else{
                        if(wylosowana > propability[i-1].prop && wylosowana <= propability[i].prop){
                            random_edge = propability[i].id;
                        }
                    }
                }
                
                for(var i=0,bStop = false ;i<connectedEdge.length && bStop === false;i++){
                    if(random_edge === connectedEdge[i].id){
                        random_edge = connectedEdge[i];
                        bStop = true;
                    }
                }
                if(random_edge !== null && typeof(random_edge) !== "undefined"){
                    if (currentNode === random_edge.target) {
                            currentNode = random_edge.source;
                    } else {
                            currentNode = random_edge.target;
                    }
                    visitedEdges.push(random_edge);
                }else{
                    return null;
                }
            } else {
                //jeżeli ilość połączonych nieodwiedzonych krawędzi
                //jest równa 0 to kończymy (nie znaleźliśmy węzła końcowego).
                //W innym przypadku wybieramy losową krawędź
                if (connectedEdge.length !== 0) {
                    random_edge = connectedEdge[Math.floor(Math.random() * connectedEdge.length)];
                    if (currentNode === random_edge.target) {
                        currentNode = random_edge.source;
                    } else {
                        currentNode = random_edge.target;
                    }
                    //dodajemy krawędź do odwiedzonych
                    visitedEdges.push(random_edge);
                }else{
                    return null;
                }
            }
            sigInst.draw();
        }while (currentNode !== endNode && connectedEdge.length !== 0);
        
        //zwracamy ścieżkę
        return visitedEdges;
    };
}

function graph(nodes, edges) {

    /**
     * Stany w jakim znajduje się obiekt
     */
    var stateForceAtlas = true;
    var stateSelectStartPoint = false;
    var stateSelectEndPoint = false;

    /**
     * Liczba węzłów (nodes) i krawędzi (edges) w grafie
     */
    this.nodes = nodes;
    this.edges = edges;

    var edgeValues = new Array();

    var startPoint;
    var endPoint;
    /**
     * Funkcja odpowiedzialna za akcję po kliknięciu na węzeł
     */
    this.onClick = function(event) {
        //alert("Click!");
        if (stateSelectStartPoint) {
            startPoint = event.content[0];
            $('#start-point-info').html(startPoint);
            alertify.success("Wybrano punkt startowy");
        } else if (stateSelectEndPoint) {
            endPoint = event.content[0];
            $('#end-point-info').html(endPoint);
            alertify.success("Wybrano punkt końcowy");
        }
    };
    /**
     * Przypisanie funkcji onClick do węzła
     */
    sigInst.bind('downnodes', this.onClick);

    this.setNodesNuber = function(n) {
        if (typeof (n) === 'undefined')
            this.nodes = 10;
    };

    this.setEdgesNumber = function(e) {
        if (typeof (e) === 'undefined')
            this.edges = 20;
    };

    this.clearGraph = function() {
        sigInst.emptyGraph();
    };
    
    this.cleanGraph = function(){
        var neighbours = new Array();
        sigInst.iterEdges(function(e){
           if(e.source === e.target){
               sigInst._core.graph.dropEdge(e.id);
           }
           if (neighbours.indexOf(e.source.id) !== -1 && neighbours[e.source.id] === e.target.id) {
                sigInst._core.graph.dropEdge(e.id);
           }else if (neighbours.indexOf(e.target.id) !== -1 && neighbours[e.target.id] === e.source.id) {
                sigInst._core.graph.dropEdge(e.id);
           }else{
                neighbours[e.source.id] = e.target.id;
           }
        });
    };

    this.chooseStartPoint = function() {
        if (stateForceAtlas) {
            this.stopForceAtlas();
            this.calculateEdgeValues();
        }
    };

    this.calculateEdgeValues = function() {
        $('#processing-modal').modal('show');
        sigInst.iterEdges(function(e) {
            nodeA = sigInst._core.graph.nodesIndex[e.source];
            nodeB = sigInst._core.graph.nodesIndex[e.target];
            edgeValues[e.id] = Math.sqrt((nodeB.x - nodeA.x) * (nodeB.x - nodeA.x) + (nodeB.y - nodeA.y) * (nodeB.y - nodeA.y));
        });
        $('#processing-modal').modal('hide');
    };

    this.generateGraph = function() {
        edgeValues = new Array();
        var i, C = 5, d = 0.5, clusters = [];
        for (i = 0; i < C; i++) {
            clusters.push({
                'id': i,
                'nodes': [],
                'color': 'rgb(' + Math.round(Math.random() * 256) + ',' +
                        Math.round(Math.random() * 256) + ',' +
                        Math.round(Math.random() * 256) + ')'
            });
        }

        for (i = 0; i < this.nodes; i++) {
            var cluster = clusters[(Math.random() * C) | 0];
            sigInst.addNode('n' + i, {
                'x': Math.random(),
                'y': Math.random(),
                'size': 0.5 + 4.5 * Math.random(),
                'color': cluster['color'],
                'cluster': cluster['id']
            });
            cluster.nodes.push('n' + i);
        }

        for (i = 0; i < this.edges; i++) {
            if (Math.random() < 1 - d) {
                sigInst.addEdge(i, 'n' + (Math.random() * this.nodes | 0), 'n' + (Math.random() * this.nodes | 0));
            } else {
                var cluster = clusters[(Math.random() * C) | 0], n = cluster.nodes.length;
                sigInst.addEdge(i, cluster.nodes[Math.random() * n | 0], cluster.nodes[Math.random() * n | 0]);
            }
        }
        this.cleanGraph();
        sigInst.draw();
        if (stateForceAtlas) {
            this.startForceAtlas();
        } else {
            this.calculateEdgeValues();
        }

    };

    this.startForceAtlas = function() {
        stateForceAtlas = true;
        sigInst.startForceAtlas2();
        $('#stop-graph').html("Zatrzymaj rozmieszczanie węzłów");
    };

    this.stopForceAtlas = function() {
        stateForceAtlas = false;
        sigInst.stopForceAtlas2();
        $('#stop-graph').html("Wznów rozmieszczanie węzłów");
    };

    this.isForceAtlasRunning = function() {
        return stateForceAtlas;
    };
    this.getStateSelectStartPoint = function() {
        return stateSelectStartPoint;
    };

    this.getStateSelectEndPoint = function() {
        return stateSelectEndPoint;
    };

    this.setStateSelectStartPoint = function(value) {
        if (value === true) {
            if (stateForceAtlas) {
                this.stopForceAtlas();
                this.calculateEdgeValues();
            }
            stateSelectStartPoint = true;
            this.setStateSelectEndPoint(false);
            $('#select-start-node').removeClass('btn-info');
            $('#select-start-node').addClass('btn-danger');
        } else {
            stateSelectStartPoint = false;
            $('#select-start-node').removeClass('btn-danger');
            $('#select-start-node').addClass('btn-info');
        }
    };
    this.setStateSelectEndPoint = function(value) {
        if (value === true) {
            if (stateForceAtlas) {
                this.stopForceAtlas();
                this.calculateEdgeValues();
            }
            stateSelectEndPoint = true;
            this.setStateSelectStartPoint(false);
            $('#select-end-node').removeClass('btn-info');
            $('#select-end-node').addClass('btn-danger');
        } else {
            stateSelectEndPoint = false;
            $('#select-end-node').removeClass('btn-danger');
            $('#select-end-node').addClass('btn-info');
        }
    };
    
    var calculateSolutionValue = function(edgeList){
        if (edgeList === null || typeof(edgeList) === "undefined") {
            return -1;
        }else{
            var cost = 0;
            for(var i=0; i < edgeList.length; i++){
                cost += edgeValues[edgeList[i].id];
            }
            return cost;
        }
    };
    
    var calculateFeromoneTrail = function(edgeList,P, Q,feromoneMatrix, calculatedCost){
        for(var i=0;i<edgeList.length;i++){
            feromoneMatrix[edgeList[i].id] = (1 - P) * feromoneMatrix[edgeList[i].id] + (Q / calculatedCost);
        }
    };
    
    this.startAlgorithm = function(iloscIt, iloscMr) {
        var wplyw_feromonow = 5;
        var wplyw_odleglosci = 2;
        var wspolczynnik_parowania = 0.2;
        var feromonPoczatkowy = 1;
        var iloscIteracji = iloscIt;
        var feromoneMatrix = [];
        var iloscMrowek = iloscMr;
        
        var solutionArray = [];
        
        var najlepszeRozwiazanieId;
        var najlepszeRozwiazanieCost = null;
        
        var errorPresence = false;
        if (typeof (startPoint) === "undefined") {
            alertify.error("Nie wybrano punktu startowego");
            errorPresence = true;
        }
        if (typeof (endPoint) === "undefined") {
            alertify.error("Nie wybrano punktu końcowego");
            errorPresence = true;
        }
        if (!errorPresence) {
            var ants = [];
            var iteracja = 0;

            sigInst.iterEdges(function(e) {
                feromoneMatrix[e.id] = feromonPoczatkowy;
            });

            
            var ant = new ANT(startPoint, endPoint, feromoneMatrix, edgeValues, wplyw_feromonow, wplyw_odleglosci);
            
            
            while(iteracja < iloscIteracji){
                
                najlepszeRozwiazanieCost = null;
                najlepszeRozwiazanieId = null;
                
                //budowa rozwiązań
                for(var ilMro=0;ilMro < iloscMrowek;ilMro++){
                    solutionArray[ilMro] = ant.search();
                }

                
                for(var i=0; i < solutionArray.length;i++){
                    var calculation = calculateSolutionValue(solutionArray[i]);
                    if(calculation !== -1){
                        calculateFeromoneTrail(solutionArray[i], wspolczynnik_parowania, 1,calculation);
                        if(najlepszeRozwiazanieCost === null || calculation < najlepszeRozwiazanieCost){
                            najlepszeRozwiazanieId = i;
                            najlepszeRozwiazanieCost = calculation;
                        }
                    }
                }
                
                console.log('id najlepszego: ' + najlepszeRozwiazanieId);
                console.log('długość ścieżki: ' + calculateSolutionValue(solutionArray[najlepszeRozwiazanieId]));
                //promowanie najlepszego rozwiązania
                for(var i=0;i < solutionArray[najlepszeRozwiazanieId].length;i++){
                    feromoneMatrix[solutionArray[najlepszeRozwiazanieId][i].id] += 3;
                }
                
                iteracja++;
            }
            console.log("---Finito---");
        }
    };
}
;